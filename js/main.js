import * as THREE from './three/three.module.js';

import { FontLoader } from './three/FontLoader.js';
import { ColladaLoader } from './three/ColladaLoader.js';
import { TextGeometry } from './three/TextGeometry.js';

let camera, scene, renderer;
let clock;
let furret, ambientLight;
let font, textMaterials;
let mouseMesh, mouseGeometry, mouseDefault, mouseDown, mouseHover;
let discordGeometry, discordMesh, discordTexture, discordButton = false, discordLink = "https://discord.gg/rQm6WxYzXA";
let youtubeGeometry, youtubeMesh, youtubeTexture, youtubeButton = false, youtubeLink = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
let isMobile;


const loader = new ColladaLoader();
loader.load('models/Furret/model.dae', function(collada) {
    furret = collada.scene;

    init();
    animate();
});



function init() {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		isMobile = true;
	}
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    clock = new THREE.Clock();
    
    // SCENE
    camera.position.z = 15;
    furret.rotation.y = Math.PI*3/2;
    ambientLight = new THREE.AmbientLight(0xffffff);
    scene.background = new THREE.TextureLoader().load("textures/background.jpg");
    scene.add(ambientLight, furret);

    // TEXT
    textMaterials = [
        new THREE.MeshPhongMaterial({ color: 0x855d4a, flatShading: true }),
        new THREE.MeshPhongMaterial({ color: 0xcebaa5 })
    ];
    loadFont()

    // BUTTONS
    discordGeometry = new THREE.BoxGeometry(3, 3, 3);
	discordTexture = new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("textures/discord.png")});
	discordMesh = new THREE.Mesh(discordGeometry, discordTexture);
	discordMesh.position.x = -18
	discordMesh.position.y = -8
	scene.add(discordMesh);

	youtubeGeometry = new THREE.BoxGeometry(3, 3, 3);
	youtubeTexture = new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("textures/youtube.jpg")});
	youtubeMesh = new THREE.Mesh(youtubeGeometry, youtubeTexture);
	youtubeMesh.position.x = 18
	youtubeMesh.position.y = -8
	scene.add(youtubeMesh);

    // MOUSE
    mouseGeometry = new THREE.SphereGeometry(0.5, 16, 16);
    mouseDefault = new THREE.MeshBasicMaterial({ color: 0x6a4a3b });
    mouseDown = new THREE.MeshBasicMaterial({ color: 0xcebaa5 });
    mouseHover = new THREE.MeshBasicMaterial({ color: 0x6a4a3b });
    mouseMesh = new THREE.Mesh(mouseGeometry, mouseDefault);
    mouseMesh.position.z = 5;
    if (isMobile) mouseMesh.visible = false;
    scene.add(mouseMesh);

    // LISTENERS
    document.body.appendChild(renderer.domElement);
    window.addEventListener('resize', onWindowResize);
    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mousedown', onMouseDown);
    document.addEventListener('mouseup', onMouseUp);
    document.addEventListener('click', onMouseClick);
}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    const delta = clock.getDelta();
    if (furret !== undefined) {
        furret.rotation.x += delta * 0.5;
        furret.rotation.y += delta * 1;
    }
	discordMesh.rotation.y += delta * 0.5;
	if (discordButton && discordMesh.scale.x > 0.6){
		let scale = discordMesh.scale;
		discordMesh.scale.set(scale.x - 0.1, scale.y - 0.1, scale.x - 0.1)
	} else if (discordButton){
		discordButton = false;
	} else if (discordMesh.scale.x < 1) {
		let scale = discordMesh.scale;
		discordMesh.scale.set(scale.x + 0.1, scale.y + 0.1, scale.x + 0.1)
	}
	youtubeMesh.rotation.y += delta * 0.5;
	if (youtubeButton && youtubeMesh.scale.x > 0.6){
		let scale = youtubeMesh.scale;
		youtubeMesh.scale.set(scale.x - 0.1, scale.y - 0.1, scale.x - 0.1)
		console.log(youtubeMesh.scale)
	} else if (youtubeButton){
		youtubeButton = false;
	} else if (youtubeMesh.scale.x < 1) {
		let scale = youtubeMesh.scale;
		youtubeMesh.scale.set(scale.x + 0.1, scale.y + 0.1, scale.x + 0.1)
	}
    renderer.render(scene, camera);
}

function loadFont() {
    const loader = new FontLoader();
    loader.load('fonts/helvetiker_regular.typeface.json', function(response) {
        font = response;
        createTexts();
    });
}

function createTexts() {
    let textFurret = createText("F u r r e t", 5, 0.1, 4, 1, 0.5, true);
    textFurret.position.x = -13
    textFurret.position.y = 20
    textFurret.position.z = -20
    scene.add(textFurret);
}

function createText(text, size, height, curveSegments, bevelThickness, bevelSize, bevelEnabled) {
    const textGeo = new TextGeometry(text, {
        font: font,
        size: size,
        height: height,
        curveSegments: curveSegments,

        bevelThickness: bevelThickness,
        bevelSize: bevelSize,
        bevelEnabled: bevelEnabled
    });
    textGeo.computeBoundingBox();

    const textMesh = new THREE.Mesh(textGeo, textMaterials);
    return textMesh;
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function onMouseMove(e) {
    e.preventDefault();

    var vector = new THREE.Vector3(
        (e.clientX / window.innerWidth) * 2 - 1,
        -(e.clientY / window.innerHeight) * 2 + 1, 0.5);

    vector.unproject(camera);
    var dir = vector.sub(camera.position).normalize();
    var distance = -camera.position.z / dir.z;
    var pos = camera.position.clone().add(dir.multiplyScalar(distance));
    mouseMesh.position.copy(pos);
}

function onMouseDown(e) {
    mouseMesh.material = mouseDown;
}

function onMouseUp(e) {
    mouseMesh.material = mouseDefault;
}

function onMouseClick(e) {
	if (mouseMesh.position.distanceTo(discordMesh.position) < 3){
		discordButton = true;
		window.open(discordLink, '_blank');
	}
	if (mouseMesh.position.distanceTo(youtubeMesh.position) < 3){
		youtubeButton = true;
		window.open(youtubeLink, '_blank');
	}
}
